from random import randint

class Dice:
    def __init__(self, nb_faces=6):
        self.__nb_faces = nb_faces
        self.value = randint(1,self.__nb_faces)

    def reroll(self):
        self.value = randint(1,self.__nb_faces)
        self.display()

    def display(self):
        print("Valeur: %s" % self.value)

    def nb_faces(self):
        return self.__nb_faces

class DiceHundred(Dice):
    def __init__(self):
        super().__init__(100)



# d1 = Dice(2)
#
# d1.display()
# d1.reroll()
# d1.reroll()
# d1.reroll()
# d1.reroll()
# d1.reroll()
# d1.reroll()
# d1.reroll()
# Dice.reroll(d1)
# print(d1.nb_faces())

# new_dice = DiceHundred()
# new_dice.reroll()
# new_dice.reroll()
# new_dice.reroll()
# new_dice.reroll()
# new_dice.reroll()
# new_dice.reroll()
#
# my_dice = Dice()
#
# Dice.reroll(my_dice)
# new_dice = DiceHundred()
#
# super(new_dice).

class Game:
    def __init__(self):
        self.dice = Dice()

    def throw_dices(self):
        self.dice.reroll()

my_game = Game()
my_game.throw_dices()

my_dice = my_game.dice
my_dice.value = 4
print(my_game.dice.value)