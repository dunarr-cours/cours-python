# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

#
# def print_hi(name):
#     # Use a breakpoint in the code line below to debug your script.
#     print(f'Hi, {name}')  # Press Ctrl+8 to toggle the breakpoint.
#
#
# # Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#     print_hi('PyCharm')
#
# # See PyCharm help at https://www.jetbrains.com/help/pycharm/
#
#
# x = 'Bonjour'  # str
# x = "Bonjour"  # str
# x = "B"  # str
#
# x = 5  # int
# x = -3  # int
#
# x = 5.3  # float
# x = -0.3  # float
#
# x = 5+3j  # complex
#
# x = True   # bool
# x = False  # bool
#
# x = ["Bonjour","Bonjour", 5, -1, [1]]  # list
#
# x = ("Bonjour", 5, -1, (5, 3), [1])  # tuple
# x = ("Bonjour",)  # tuple
#
# x = {"Bonjour", "Bonjour", 5, -1}  # set
#
# x = {"prenom": "john", "nom": "Rambo", 5: 10}  # dict
#
#
# test = "Bonjour"
#
# if "Bonjour" == test:
#     print("Hello")
#     print(x)
# elif x < 10:
#     print('Deuxième condition vraie')
# else:
#     print("Condition fausse")
#
# i = 0
# while i < 10:
#     i += 1
#     print(i)
#
# maliste = ["Bojour", 5, 3, -1, "test"]
#
# for mot in maliste:
#     print(mot)
#
# print("Fin")
#
# x = range(10)
# x = range(5, 10)
#
# for i in x:
#     print(i)
#
# for i in range(10):  # equivalent de for(i = 0; i<=10; i++)
#     print(i)
#
# for i in range(5, 10, 3):  # equivalent de for(i = 5; i<=10; i+=3)
#     print(i)
#
# print(x)
#
# a = 1
# b = 3
# c = 5
#
# print(a < b < c)
#
# tableau = ["Bonjour", 5, -1 , 0.5, 'Salut', -1]
#
# print(len(tableau))  # longueure du tableau
#
# print(tableau[2])  # récuperation d'un element
# print(tableau[-2])  # en partant de la fin
#
# tableau.append("new el")  # ajout d'un élément
# print(tableau)
#
# tableau.insert(2, "4")
# tableau.pop(0)
# tableau.remove('Salut')
#
#  # https://www.w3schools.com/python/python_arrays.asp
#
# print(0.5 in tableau)  # est-ce qu'un ele est dans mon tableau
#
# tableau2 = [0, 2, 4, 6, 8, 10, 12, 14]
#
# print(tableau2[:4])  # 4 premiers
# print(tableau2[:-2])  # sauf les deux derniers
#
# print(tableau2[4:]) # tout sauf les 4 premiers
#
# print(tableau2[4:6])
#
#
# print([i*2 for i in tableau2])  # map
#
# print(tableau2 * 5)
#
# print("Bonjour " * 50)
#
# print("5"*5)  # /!\
#
# print("hello " + "World")
# print("5" + "5")
# a = "5"
# b = "8"
# print(int(a)+int(b))
# print(int(5.6))

def affiche_message(first_name="John", last_name="Rambo"):
    print("Bonjour " + first_name + " " + last_name)
    a = 5
    b = 3
    return a, b

affiche_message("William", "Lefebvre")
affiche_message()
affiche_message("William")
a = affiche_message(last_name="Lefebvre")
print(a)

a = 5
b = 6
# c = a
# d = b

# c, d = a, b

# c = a
# a = b
# b = c
#
# a, b = b, a


var1, var2 = affiche_message()
type(1)

x = "Hello"
x = 'Hello'
x = ('Lorem ipsum dolor sit amet, consectetur'
     ' adipiscing elit. Maecenas vel fermentum'
     ' tellus. Morbi malesuada eleifend ipsum,'
     ' vitae suscipit nunc ultricies et.'
     ' Nam lacinia felis vitae enim sagittis,'
     ' ac rhoncus dui ornare. Maecenas luctus facilisis euismod. Nulla lectus quam, cursus id egestas vitae, finibus eu nibh. Duis eget urna felis. Aliquam et ipsum eget arcu fringilla suscipit. Aliquam suscipit diam felis, nec ornare dolor sagittis ac. Sed nibh libero, egestas vel iaculis quis, ornare at eros. Mauris urna mauris, scelerisque condimentum arcu eu, consequat lobortis erat. Curabitur orci arcu, malesuada et sodales ut, fringilla eget sapien. Aenean commodo lacus quis tincidunt dapibus. Mauris nec vehicula libero. ')

'''
fsdfhjdg
fgdfg'dfg
dhdghggdshfgdsfh
ghjgjgjhkg
'''

# print(x)
#
# x = "Lorem ipsum dolor sit amet"
# print("Ips" in x)

mot = "William"

print(f"Bonjour {mot}")


x = "Bonjour je m'appel %s %s"

print(x % ("William", "Lefebvre"))
print("Bonjour je m'appel %s %s" % ("William", "Lefebvre"))

x = "Bonjour {} {}"
print(x.format("William", "Lefebvre"))

x = "Bonjour {first_name} {last_name}"
print(x.format(first_name="William", last_name="Lefebvre"))

print(1,"Bonjour", -1)

first_name = input("Votre prénom: \n")
print("bonjour %s" % first_name)