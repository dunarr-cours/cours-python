from mysql import connector
import params
from dotenv import load_dotenv
from os import getenv
load_dotenv()

print(getenv("DB_NAME"))




db = connector.connect(
    host="localhost",
    database=getenv('DB_NAME'),
    user=getenv("DB_USER"),
    password=getenv("DB_PASS")
)
cursor = db.cursor(dictionary=True)

# username = "dunarr2"
# first_name = "William"
# last_name = "Lefebvre"
#
# cursor.execute("INSERT INTO user (username, first_name, last_name) VALUES (%s, %s, %s)", (username, first_name, last_name))
# cursor.execute(
#     "INSERT INTO user (username, first_name, last_name) VALUES (%(user_username)s, %(user_first_name)s, %(user_last_name)s)",
#     {"user_username": username, "user_first_name": first_name, "user_last_name": last_name}
# )
# db.commit()


# print(cursor.fetchall())

query = "SELECT username FROM user WHERE id = %(id)s"

cursor.execute(query, {"id":3})

user = cursor.fetchone()
print(user["username"])
# while True:
#     user = cursor.fetchone()
#     if user == None:
#         break
#     else:
#         print(user['username'])
